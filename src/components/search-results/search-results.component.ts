import {Component, Input, OnInit} from '@angular/core';
import {Spare} from "../../shared/data";

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit {

  constructor() { }

  @Input()
  public set results(value: Spare[]) {
    this._results = value;
  }

  public get results() {
    return this._results;
  }

  ngOnInit(): void {
  }

  private _results: Spare[];
}
