import {NgModule} from '@angular/core';
import {SharedModule} from "../../shared";
import {MainPageComponent} from "./main-page.component";
import {MainPageRouting} from "./main-page.routing";
import {SearchFormComponent} from "../search-form";
import {SearchResultsComponent} from "../search-results";
import {NgxSpinnerModule} from "ngx-spinner";


@NgModule({
  imports: [
    SharedModule,
    MainPageRouting,
    NgxSpinnerModule
  ],
  exports: [],
  declarations: [
    MainPageComponent,
    SearchFormComponent,
    SearchResultsComponent
  ]
})
export class MainPageModule {
}
