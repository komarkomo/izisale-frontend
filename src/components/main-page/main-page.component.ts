import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {Spare} from "../../shared/data";
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainPageComponent {
  constructor(private cdr: ChangeDetectorRef, private spinner: NgxSpinnerService) {
  }

  public resultList: Spare[];
  public isNoMatches: boolean;
  public nothingToShow: boolean = true;
  public isLoading: boolean = false;

  public checkResults(results: Spare[]): void {
    if (results == null) {
      this.isNoMatches = null;
      this.nothingToShow = true;
      this.cdr.markForCheck();
      return;
    }
    results.length ? this.isNoMatches = false : this.isNoMatches = true;
    this.nothingToShow = false;
    this.resultList = results;
    this.cdr.markForCheck();
  }

  public isSearchStarted(pending): void {
    this.isLoading = pending;
    if (pending) {
      this.spinner.show();
    } else {
      this.spinner.hide();
    }
  }
}
