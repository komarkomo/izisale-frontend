import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {AddressService, WebsocketService} from "../../shared/services";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Spare} from "../../shared/data";

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit, OnDestroy {
  @Output()
  results = new EventEmitter<Spare[]>();

  @Output()
  loading = new EventEmitter<boolean>();

  private clientId: number;
  public searchForm: FormGroup;
  public resultList: Spare[];
  public isDisabled: boolean = false;

  constructor(private wsService: WebsocketService,
              private addressService: AddressService,
              private fb: FormBuilder) { }

  public ngOnInit() {
    this.searchForm = this.fb.group({
      searchWord: [null, [Validators.required, Validators.minLength(2)]]
    });
    this.wsService.createWebSocket();
  }

  public ngOnDestroy(): void {
  }

  public startSearch(): void {
    const searchWord = this.searchForm.getRawValue().searchWord;
    if (!searchWord) {
      this.resultList = null;
      this.results.emit(this.resultList);
      return;
    }
    this.isDisabled = true;
    this.loading.emit(true);
    this.clientId = this.wsService.clientId;
    this.wsService.searchByVendorCode(this.clientId, searchWord);
    this.getResponse();
  }


  private getResponse(): void {
    this.wsService.stompClient.subscribe(`/aggregator/ws/${this.clientId}`, frame => {
      this.resultList = JSON.parse(frame.body).responseBody.spares;
      this.results.emit(this.resultList);
      this.loading.emit(false);
      this.isDisabled = false;
      console.log(this.resultList);
    });
  }

}
