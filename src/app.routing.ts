import {RouterModule, Routes} from '@angular/router';
import {WrapperComponent} from './shared/layouts/wrapper';
// import {AuthComponent} from './pages/auth/auth.component';
// import {AuthGuard} from './shared/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: WrapperComponent,
    // canActivate: [AuthGuard],
    children: [
      {
        path: '',
        // canActivate: [AuthGuard],
        loadChildren: () => import('./components/main-page/main-page.module').then(m => m.MainPageModule),
      },
      // {
      //   path: 'loading-data',
      //   canActivate: [AuthGuard],
      //   loadChildren: () => import('./pages/loading-data/loading-data.module').then(m => m.LoadingDataModule),
      // },
      // {
      //   path: 'orders-basket',
      //   canActivate: [AuthGuard],
      //   loadChildren: () => import('./pages/orders-basket/orders-basket.module').then(m => m.OrdersBasketModule),
      // },
      // {
      //   path: 'clients',
      //   canActivate: [AuthGuard],
      //   loadChildren: () => import('./pages/clients/clients.module').then(m => m.ClientsModule),
      // },
      // {
      //   path: 'providers',
      //   canActivate: [AuthGuard],
      //   loadChildren: () => import('./pages/providers/providers.module').then(m => m.ProvidersModule),
      // },
    ]
  },
];

// must use {initialNavigation: 'enabled'}) - for one load page, without reload
export const AppRoutes = RouterModule.forRoot(routes, {initialNavigation: 'enabled'});
