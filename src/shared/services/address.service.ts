import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AddressService {
  private serverUrl = `http://194.67.105.31:8080`;

  getServerUrl(): string {
    return this.serverUrl;
  }

}
