import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AddressService} from './address.service';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';

@Injectable({providedIn: 'root'})
export class WebsocketService {
  private readonly baseUrl;
  public stompClient: any;
  public clientId;

  constructor(private httpClient: HttpClient,
              private addressService: AddressService) {
    this.baseUrl = addressService.getServerUrl() + '/aggregator';
  }

  createWebSocket() {
    const ws = new SockJS(`${this.addressService.getServerUrl()}/websocket`);
    this.stompClient = Stomp.over(ws);
    this.stompClient.debug = null;
    this.stompClient.connect({}, () => {
      this.stompClient.subscribe('/aggregator/registration', message => {
        this.clientId = JSON.parse(message.body).responseBody.clientId;
      });
      this.stompClient.send(`/register-client`);
    });
  }

  searchByVendorCode(id, code): void {
    this.stompClient.send('/search', {}, JSON.stringify({
      clientId: id,
      vendorCode: code
    }));
  }

}
