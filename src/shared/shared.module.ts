import {CommonModule} from '@angular/common';
import {NgModule, ModuleWithProviders} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {LayoutsModule} from "./layouts";

@NgModule({
  imports: [],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutsModule
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {ngModule: SharedModule};
  }
}
