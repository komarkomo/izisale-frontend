export interface Spare {
  count: number;
  id?: number;
  name: string;
  originalPrice?: number;
  price: number;
  producer: string;
  vendorCode: string;
}
