import {FooterComponent} from "./footer";
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {HeaderComponent} from "./header";
import {WrapperComponent} from "./wrapper";
import {AngularSvgIconModule} from 'angular-svg-icon';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    AngularSvgIconModule.forRoot()
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    WrapperComponent
  ],
  exports: [WrapperComponent, HeaderComponent, FooterComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LayoutsModule {
}
